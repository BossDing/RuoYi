package com.ruoyi.web.controller.blog;

import com.ruoyi.blog.domain.Category;
import com.ruoyi.blog.service.CategoryService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @className: CategoryController
 * @description:
 * @auther: DogJay
 * @Date: 2019/3/16
 * @Version: 1.0
 */
@Controller
@RequestMapping("/blog/category")
public class CategoryController extends BaseController {

    @Autowired
    CategoryService categoryService;


    @RequiresPermissions("blog:category:view")
    @GetMapping()
    public String category() {
        return "blog/category/category";
    }

    @GetMapping("/list")
    @RequiresPermissions("blog:category:list")
    @ResponseBody
    public TableDataInfo list(Category category) {
        startPage();
        List<Category> list = categoryService.selectCategoryList(category);
        return getDataTable(list);
    }

    @GetMapping("/add")
    public String add() {
        return "blog/category/add";
    }

    @Log(title = "分类管理", businessType = BusinessType.INSERT)
    @RequiresPermissions("blog:category:add")
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Category category) {
        return toAjax(categoryService.insertCategory(category));
    }

    @GetMapping("/edit/{categoryId}")
    public String edit(@PathVariable Integer categoryId, Model model) {
        model.addAttribute("category", categoryService.selectCategoryById(categoryId));
        return "blog/category/edit";
    }

    @PutMapping("/edit")
    @RequiresPermissions("blog:category:edit")
    @Log(title = "分类管理", businessType = BusinessType.UPDATE)
    @ResponseBody
    public AjaxResult editSave(Category category) {
        return toAjax(categoryService.updateCategory(category));
    }

    @DeleteMapping("/remove")
    @Log(title = "分类管理", businessType = BusinessType.DELETE)
    @RequiresPermissions("blog:category:remove")
    @ResponseBody
    public AjaxResult remove(Integer[] ids) {
        return toAjax(categoryService.deleteCategoryByIds(ids));
    }

    @PutMapping("/support/{support}")
    @RequiresPermissions("blog:category:support")
    @Log(title = "分类管理", businessType = BusinessType.UPDATE)
    @ResponseBody
    public AjaxResult supportSave(Integer categoryId, @PathVariable String support) {
        return toAjax(categoryService.updateCategorySupportById(categoryId, support));
    }

    @PostMapping("/checkCategoryTitleUnique")
    @ResponseBody
    public String checkCategoryTitleUnique(String title) {
        return categoryService.checkCategoryTitleUnique(title);
    }

}
