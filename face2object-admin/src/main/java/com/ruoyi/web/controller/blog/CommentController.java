/*
 * Copyright (C) 2017--2019, 微笑的丁总 All rights reserved
 * Author: DingJie
 * FileName: CommentController
 * Date: 2019/5/8 7:07 PM
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号             描述
 */
package com.ruoyi.web.controller.blog;

import com.ruoyi.blog.domain.Comment;
import com.ruoyi.blog.service.CommentService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 *
 * @author dogjay
 * @create 2019/5/8
 * @since 1.0.0
 */
@Controller
@RequestMapping("/blog/comment")
public class CommentController extends BaseController {
    
    @Autowired
    CommentService commentService;

    @RequiresPermissions("blog:comment:view")
    @GetMapping()
    public String comment() {
        return "blog/comment/comment";
    }


    @GetMapping("/list")
    @RequiresPermissions("blog:comment:list")
    @ResponseBody
    public TableDataInfo list(Comment comment) {
        startPage();
        List<Comment> list = commentService.selectCommentList(comment);
        return getDataTable(list);
    }


    @DeleteMapping("/remove")
    @Log(title = "评论管理", businessType = BusinessType.DELETE)
    @RequiresPermissions("blog:comment:remove")
    @ResponseBody
    public AjaxResult remove(Integer[] ids) {
        return toAjax(commentService.deleteCommentByIds(ids));
    }

}
