package com.ruoyi.web.controller.front;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ruoyi.blog.domain.Blog;
import com.ruoyi.blog.domain.Comment;
import com.ruoyi.blog.service.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.constant.CommonConstant;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.AddressUtils;
import com.ruoyi.common.utils.Face2ObjectUtils;
import com.ruoyi.framework.web.service.DictService;
import com.ruoyi.link.domain.Link;
import com.ruoyi.link.service.LinkService;
import com.ruoyi.system.service.CarouselMapService;
import com.ruoyi.system.service.INoticeService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @className: HomeController
 * @description: 前台首页Controller
 * @auther: DogJay
 * @date: 03/27/19
 * @version: 1.0
 */
@Controller
public class HomeController extends BaseController {

    @Autowired
    HomeService homeService;
    @Autowired
    BlogService blogService;
    @Autowired
    CategoryService categoryService;
    @Autowired
    TagService tagService;
    @Autowired
    LinkService linkService;
    @Autowired
    INoticeService noticeService;
    @Autowired
    CarouselMapService carouselMapService;

    @Autowired
    CommentService commentService;

    @Autowired
    DictService dictService;

    @Autowired
    AlbumService albumService;

//    @Autowired
//    EsBlogService esBlogService;
    
//    @Autowired
//    FriendsService friendsService;

    /**
     * 设置前台页面公用的部分代码 均设置Redis缓存
     */
    private void setCommonMessage(Model model) {
        //获取分类下拉项中的分类
        model.addAttribute("categories", categoryService.selectSupportCategoryList());
        //查询所有的标签
//        model.addAttribute("tags", tagService.selectTagList(new Tag()));
        
        //查询最近更新的文章
        model.addAttribute("newestUpdateBlog", blogService.selectNewestUpdateBlog());
        //查询文章排行
        model.addAttribute("blogRanking", blogService.selectBlogRanking());
        //查询推荐博文
        model.addAttribute("supportBlog", blogService.selectSupportBlog());
        //查询通知
//        model.addAttribute("notices", noticeService.selectNoticeListDisplay());
        //获取友链信息
        model.addAttribute("links", linkService.selectLinkListFront());
//        long start = System.currentTimeMillis();
        //本站专辑
        model.addAttribute("albums", albumService.selectFrontAlbumList());
//        System.out.println(System.currentTimeMillis()-start);
    }

    /**
     * 前台首页
     */
    @GetMapping("/")
    @Log(title = "首页")
    public String index(Integer pageNum, Model model) {
        setCommonMessage(model);
        PageHelper.startPage(pageNum == null ? 1 : pageNum, 10, "create_time desc");
        model.addAttribute("blogs", new PageInfo<>(homeService.selectFrontBlogList(new Blog())));
        //放置轮播图
        model.addAttribute("carouselMaps", carouselMapService.selectCarouselMapListFront());
        return "front/index";
    }

    @GetMapping("/carouselMap/{carouselId}")
    public String carouselMapI(@PathVariable Integer carouselId, String url) {
        //增加点击量
        carouselMapService.incrementCarouselClickById(carouselId);
        return redirect(url);
    }

    /**
     * 关于我
     */
    @Log(title = "关于我")
    @GetMapping("/about.html")
    public String about(Model model) {
        setCommonMessage(model);
        return "front/about";
    }

    /**
     * 归档
     */
    @Log(title = "归档")
    @GetMapping("/archives.html")
    public String archives(Model model) {
        setCommonMessage(model);
        model.addAttribute("archives", homeService.selectArchives());
        return "front/archives";
    }
    
    @Log(title = "博客")
    @GetMapping("/article/{blogId}.html")
    public String article(@PathVariable Integer blogId, Model model) {
        setCommonMessage(model);
        Blog blog = blogService.selectBlogWithTextAndTagsAndCategoryByBlogId(blogId);
        //只能访问是已经发表的文章
        if (!CommonConstant.one.equals(blog.getStatus())) {
            return "error/404";
        }
        model.addAttribute("blog", blog);
        model.addAttribute("nextBlog", blogService.selectNextBlogById(blogId));
        model.addAttribute("previousBlog", blogService.selectPreviousBlogById(blogId));
        model.addAttribute("randBlogList", blogService.selectRandBlogList());
        model.addAttribute("commentList", commentService.selectCommentByBlogId(blogId));
        return "front/article";
    }
    
    @Log(title = "分类")
    @GetMapping({"/category/{categoryId}.html"})
    public String category(@PathVariable Integer categoryId, Integer pageNum, Model model) {
        setCommonMessage(model);
        model.addAttribute("category", categoryService.selectCategoryById(categoryId));
        Blog blog = new Blog();
        blog.setCategoryId(categoryId);
        PageHelper.startPage(pageNum == null ? 1 : pageNum, 18, "create_time desc");
        model.addAttribute("blogs", new PageInfo<>(homeService.selectFrontBlogList(blog)));
        return "front/category";
    }

    @Log(title = "标签")
    @GetMapping("/tag/{tagId}.html")
    public String tag(@PathVariable Integer tagId, Integer pageNum, Model model) {
        setCommonMessage(model);
        PageHelper.startPage(pageNum == null ? 1 : pageNum, 18, "b.create_time desc");
        List<Blog> blogs = blogService.selectBlogListByTagId(tagId);
        model.addAttribute("blogs", new PageInfo(blogs));
        model.addAttribute("tag", tagService.selectTagById(tagId));
        return "front/tag";
    }

    @Log(title = "搜索")
    @GetMapping("/search/{keyWord}.html")
    public String search(@PathVariable String keyWord, Integer pageNum, Model model) {
        setCommonMessage(model);
//        PageHelper.startPage(pageNum == null ? 1 : pageNum, 18, "create_time desc");
//        Blog blog = new Blog();
//        blog.setTitle(keyWord);
//        List<Blog> blogs = blogService.selectBlogList(blog);
//        Pageable pageable = PageRequest.of(0, 100);
//        Page<BlogDto> blogDtoPage = esBlogService.getEsBlogByKeys(keyWord,pageable);
//        Iterator<BlogDto> blogDtoIterator = esBlogService.getEsBlogByKeys(keyWord, pageable).iterator();
//        List<BlogDto> blogDtoList = new ArrayList<BlogDto>();
//        while (blogDtoIterator.hasNext()) {
//            blogDtoList.add(blogDtoIterator.next());
//        }
//        model.addAttribute("blogs", blogDtoList);
//        model.addAttribute("keyWord", keyWord);
        return "front/search";
    }

    /**
     * 留言
     */
    @Log(title = "留言")
    @GetMapping("/leaveComment.html")
    public String leaveComment(Model model) {
        setCommonMessage(model);
        return "front/leaveComment";
    }

    /**
     * 版权声明
     */
    @Log(title = "版权声明")
    @GetMapping("/copyright.html")
    public String copyright(Model model) {
        setCommonMessage(model);
        return "front/other/copyright";
    }

    /**
     * 侵删联系
     */
    @Log(title = "侵删联系")
    @GetMapping("/delete.html")
    public String delete(Model model) {
        setCommonMessage(model);
        return "front/other/delete";
    }

    /**
     * 友链显示
     */
    @Log(title = "友链显示")
    @GetMapping("/link.html")
    public String friendLink(Model model) {
        setCommonMessage(model);
        model.addAttribute("links", linkService.selectLinkListFrontWithSummary());
        return "front/link";
    }

    /**
     * 友链跳转
     */
    @Log(title = "友链跳转")
    @GetMapping("/linkRedirect")
    public String redirectTo(String ref, Integer id) {
        //增加点击量
        linkService.incrementLinkClickById(id);
        return redirect(ref);
    }

    /**
     * 申请友链
     */
    @PostMapping("/applyLink")
    @ResponseBody
    public AjaxResult applyLink(Link link) {
        linkService.applyLink(link);
        return AjaxResult.success();
    }

    @PostMapping("/submitComment")
    @ResponseBody
    public AjaxResult submitComment(HttpServletRequest request, @RequestParam(value = "email") String email, @RequestParam(value = "site") String site, @RequestParam(value = "content") String content, @RequestParam(value = "orderType") String orderType, @RequestParam(value = "browser") String browser, @RequestParam(value = "blogId") Long blogId) {
        Blog blog = blogService.selectBlogById(blogId.intValue());
        if (null == blog) {
            return AjaxResult.error("404", "文章不存在!");
        }
        Comment comment = new Comment();
        comment.setArticleId(blogId);
        comment.setContent(content);
        comment.setOrderType(orderType);
        comment.setEmail(email);
        comment.setSite(site);
        comment.setAvatar(Face2ObjectUtils.gravatar(email));
        comment.setIp(AddressUtils.getIpAddr(request));
        comment.setCity(AddressUtils.getRealAddressByIP(comment.getIp()));
        comment.setBrowser(browser);
        try {
            commentService.insertComment(comment);
            blogService.increamentBlogComment(blogId.intValue());
            return AjaxResult.success("博主已经收到您的消息啦，会再接再厉！");
        } catch (Exception e) {
//            e.printStackTrace();
            return AjaxResult.error(e.getMessage());
        }
    }

    /**
     * @param blogId
     * @return
     */
    @GetMapping("/getComment")
    @ResponseBody
    public AjaxResult getComment(@RequestParam(value = "blogId") Integer blogId) {
//        Blog blog = blogService.selectBlogById(blogId);
//        if (null == blog) {
//            return AjaxResult.error(404, "文章不存在!");
//        }
//        List<Comment> commentList = commentService.selectCommentByBlogId(blogId);
//        if (!CollectionUtils.isNotEmpty(commentList)) {
//            return AjaxResult.error(410, "当前文章无评论!");
//        }
//        return AjaxResult.success().put("data", commentList);
        return null;
    }

    /**
     * @return
     */
    @GetMapping("/{wechatUri}.txt")
    @ResponseBody
    public String wechatUri(@PathVariable String wechatUri) {
        return dictService.getLabel("wechatUri", wechatUri);
    }
    
    @GetMapping("/bdunion.txt")
    @ResponseBody
    public String bdunionTxt() {
        return dictService.getLabel("seo_type", "seo_bdunion");
    }

    /**
     * 专辑显示
     *
     * @param model
     */
    @Log(title = "专辑显示")
    @GetMapping("/albums.html")
    public String albums(Model model) {
        setCommonMessage(model);
        model.addAttribute("albums", albumService.selectFrontAlbumList());
        return "front/albums";
    }

    /**
     * @param albumId
     * @param pageNum
     * @param model
     * @return
     */
    @Log(title = "专辑")
    @GetMapping({"/album/{albumId}.html"})
    public String album(@PathVariable Integer albumId, Integer pageNum, Model model) {
        setCommonMessage(model);
        model.addAttribute("album", albumService.selectAlbumById(albumId));
        Blog blog = new Blog();
        blog.setAlbumId(albumId);
        PageHelper.startPage(pageNum == null ? 1 : pageNum, 18, "create_time desc");
        model.addAttribute("blogs", new PageInfo<>(homeService.selectAlbumBlogList(blog)));
        return "front/album";
    }
    
//    @ResponseBody
//    @RequestMapping("/friends")
//    public List<com.bossding.project.friends.domain.Friends> getDesktop(@RequestParam("name") String name, HttpServletResponse response){
//        response.setHeader("Access-Control-Allow-Origin", "*");
//        List<com.bossding.project.friends.domain.Friends> friendsList = friendsService.selectBlogList(name);
//        return friendsList;
//    }

}
