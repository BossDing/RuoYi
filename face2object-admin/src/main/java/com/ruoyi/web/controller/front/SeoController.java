/*
 * Copyright (C) 2017--2019, 微笑的丁总 All rights reserved
 * Author: DingJie
 * FileName: SeoController
 * Date: 2019/5/29 8:16 PM
 * Description: SEO优化Controller
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号             描述
 */
package com.ruoyi.web.controller.front;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 〈一句话功能简述〉<br> SEO优化Controller
 *
 * @author dogjay
 * @create 2019/5/29
 * @since 1.0.0
 */
public class SeoController extends BaseController {
    @GetMapping(value = "/sitemap.xml", produces = {"application/xml"})
    @Log(title = "查看sitemap.xml")
    public String sitemapXml() {
        
//        return getSitemap(TemplateKeyEnum.TM_SITEMAP_XML);
        return "";
        
    }

//    @GetMapping(value = "/sitemap.txt", produces = {"text/plain"})
//    @VLog(title = "查看sitemap.txt")
//    public String sitemapTxt() {
//        return getSitemap(TemplateKeyEnum.TM_SITEMAP_TXT);
        
//    }
//
//    @GetMapping(value = "/sitemap.html", produces = {"text/html"})
//    @VLog(value = "查看sitemap.html")
//    public String sitemapHtml() {
//        return getSitemap(TemplateKeyEnum.TM_SITEMAP_HTML);
        
//    }

    @GetMapping(value = "/robots.txt", produces = {"text/plain"})
    @Log(title = "查看robots")
    public String robots() {
//        Template template = templateService.getTemplate(TemplateKeyEnum.TM_ROBOTS);
//        Map<String, Object> map = new HashMap<>();
//        map.put("config", configService.getConfigs());
//        return FreeMarkerUtil.template2String(template.getRefValue(), map, true);
        return "";
        
    }

    private String getSitemap() {
//        Map<String, Object> map = new HashMap<>();
//        map.put("articleTypeList", typeService.listAll());
//        map.put("articleTagsList", tagsService.listAll());
//        map.put("articleList", articleService.listAll());
//        map.put("config", configService.getConfigs());
//        return FreemarkerUtil.template2String(template.getRefValue(), map, true);
        return "";
    }
}
