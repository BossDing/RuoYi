package com.ruoyi.web.controller.blog;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.blog.domain.Album;
import com.ruoyi.blog.domain.Blog;
import com.ruoyi.blog.domain.Category;
import com.ruoyi.blog.service.AlbumService;
import com.ruoyi.blog.service.BlogService;
import com.ruoyi.blog.service.CategoryService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.constant.BlogConstants;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BaiduPushTypeEnum;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.Face2ObjectUtils;
import com.ruoyi.common.utils.UrlBuildUtil;
import com.ruoyi.common.utils.baidu.BaiduPushUtil;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//import com.bossding.project.blog.blog.mapper.BlogRepository;

/**
 * @className: BlogController
 * @description: 博客处理Controller
 * @auther: DogJay
 * @Date: 2019/3/16
 * @Version: 1.0
 */
@Controller
@RequestMapping("/blog/blog")
public class BlogController extends BaseController {

    @Value("${ruoyi.siteUrl}")
    String siteUrl;

    @Value("${baidu.zhanzhang.token}")
    String baiduZhanZhangToken;

    @Autowired
    BlogService blogService;

    @Autowired
    CategoryService categoryService;

    @Autowired
    AlbumService albumService;

    @RequiresPermissions("blog:blog:view")
    @GetMapping()
    public String blog(Model model) {
        model.addAttribute("total", blogService.selectBlogCountByStatus(BlogConstants.BLOG_TOTAL));
        model.addAttribute("published", blogService.selectBlogCountByStatus(BlogConstants.BLOG_PUBLISHED));
        model.addAttribute("draft", blogService.selectBlogCountByStatus(BlogConstants.BLOG_DRAFT));
        model.addAttribute("garbage", blogService.selectBlogCountByStatus(BlogConstants.BLOG_GARBAGE));
        return "blog/blog/blog";
    }

    @GetMapping("/img")
    public String img() {
        return "blog/blog/img";
    }

    @GetMapping("/list")
    @RequiresPermissions("blog:blog:list")
    @ResponseBody
    public TableDataInfo list(Blog blog) {
        startPage();
        List<Blog> list = blogService.selectBlogList(blog);
        return getDataTable(list);
    }

    @GetMapping("/add")
    public String add(Model model) {
        model.addAttribute("categories", categoryService.selectCategoryList(new Category()));
        model.addAttribute("albums", albumService.selectAlbumList(new Album()));
        return "blog/blog/add";
    }

    @Log(title = "博客管理", businessType = BusinessType.INSERT)
    @RequiresPermissions("blog:blog:add")
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Blog blog) {
        blog.setHeaderImg(Face2ObjectUtils.getFirstImgSrc(Face2ObjectUtils.getImgStr(blog.getContent())));
        int i = blogService.insertBlog(blog);
        if (null != blog.getAlbumId()) {
            albumService.updateAlbumCountById(blog.getAlbumId());
        }
        return toAjax(i);
    }

    @GetMapping("/edit/{blogId}")
    public String edit(@PathVariable Integer blogId, Model model) {
        model.addAttribute("blog", blogService.selectBlogWithTextAndTagsAndCategoryByBlogId(blogId));
        model.addAttribute("categories", categoryService.selectCategoryList(new Category()));
        model.addAttribute("albums", albumService.selectAlbumList(new Album()));
        return "blog/blog/edit";
    }

    @PutMapping("/edit")
    @RequiresPermissions("blog:blog:edit")
    @Log(title = "博客管理", businessType = BusinessType.UPDATE)
    @ResponseBody
    public AjaxResult editSave(Blog blog) {
        Blog blogTemp = blogService.selectBlogById(blog.getBlogId());
        if (null == blogTemp) {
            return AjaxResult.error("操作异常，文章不存在！");
        }
        //如果原来文章没有专辑
        if (null == blogTemp.getAlbumId()) {
            
        } else {
            if (blogTemp.getAlbumId().intValue() != blog.getAlbumId().intValue()) {
                albumService.updateAlbumCountById(blog.getAlbumId());
                albumService.minusAlbumCountById(blogTemp.getAlbumId());
            }
        }
        return toAjax(blogService.updateBlog(blog));
    }

    @PutMapping("/support/{support}")
    @RequiresPermissions("blog:blog:support")
    @Log(title = "博客管理", businessType = BusinessType.UPDATE)
    @ResponseBody
    public AjaxResult supportSave(Integer blogId, @PathVariable String support) {
        return toAjax(blogService.updateBlogSupportById(blogId, support));
    }

    @PutMapping("/status/{status}")
    @RequiresPermissions("blog:blog:status")
    @Log(title = "博客管理", businessType = BusinessType.UPDATE)
    @ResponseBody
    public AjaxResult statusSave(String blogIds, @PathVariable String status) {
        return toAjax(blogService.updateBlogStatusById(blogIds, status));
    }

    @DeleteMapping("/remove")
    @Log(title = "博客管理", businessType = BusinessType.DELETE)
    @RequiresPermissions("blog:blog:remove")
    @ResponseBody
    public AjaxResult remove(Integer[] ids) {
        return toAjax(blogService.deleteBlogById(ids));
    }

//    @PostMapping("/synchronize")
//    @Log(title = "博客管理", businessType = BusinessType.DELETE)
//    @RequiresPermissions("blog:blog:synchronize")
//    @ResponseBody
//    public AjaxResult synchronizeBlogs() {
//        try {
//            blogService.synchronizeBlogs();
//            return AjaxResult.success("同步文章到ES成功！");
//        } catch (Exception e) {
//            return AjaxResult.error("同步文章到ES失败，系统异常！");
//        }
//    }

    @PostMapping("/pushBlogs2Baidu")
    @Log(title = "博客管理", businessType = BusinessType.DELETE)
    @RequiresPermissions("blog:blog:push")
    @ResponseBody
    public AjaxResult pushBlogs2Baidu() {
        try {
            List<Integer> blogIds = blogService.getAllIds();
            StringBuilder params = new StringBuilder();
            for (Integer id : blogIds) {
                params.append(siteUrl).append("/article/").append(id).append(".html\n");
                params.append(siteUrl).append("/article/").append(id).append(".html\n");
            }

            System.out.println(baiduZhanZhangToken);
            // urls: 推送, update: 更新, del: 删除
            String url = UrlBuildUtil.getBaiduPushUrl(BaiduPushTypeEnum.urls.toString(), siteUrl, baiduZhanZhangToken);
            /**
             * success	       	int	    成功推送的url条数
             * remain	       	int	    当天剩余的可推送url条数
             * not_same_site	array	由于不是本站url而未处理的url列表
             * not_valid	   	array	不合法的url列表
             */
            // {"remain":4999997,"success":1,"not_same_site":[],"not_valid":[]}
            /**
             * error	是	int	      错误码，与状态码相同
             * message	是	string	  错误描述
             */
            //{error":401,"message":"token is not valid"}
            String result = BaiduPushUtil.doPush(url, params.toString());
//        log.info(result);
            JSONObject resultJson = JSONObject.parseObject(result);
            if (resultJson.containsKey("error")) {
                return AjaxResult.error(resultJson.getString("message"));
            }
            return AjaxResult.success("推送文章到百度成功！");
        } catch (Exception e) {
            return AjaxResult.error("推送文章到百度失败，系统异常！");
        }
    }


}
