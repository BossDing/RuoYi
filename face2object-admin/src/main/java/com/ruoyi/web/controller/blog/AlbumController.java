/*
 * Copyright (C) 2017--2019, 微笑的丁总 All rights reserved
 * Author: DingJie
 * FileName: AlbumController
 * Date: 2019/5/15 4:42 PM
 * Description: 专辑Controller
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号             描述
 */
package com.ruoyi.web.controller.blog;


import com.ruoyi.blog.domain.Album;
import com.ruoyi.blog.service.AlbumService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 〈一句话功能简述〉<br> 专辑Controller
 *
 * @author dogjay
 * @create 2019/5/15
 * @since 1.0.0
 */
@Controller
@RequestMapping("/blog/album")
public class AlbumController extends BaseController {
    @Autowired
    AlbumService albumService;

    @RequiresPermissions("blog:album:view")
    @GetMapping()
    public String album() {
        return "blog/album/album";
    }

    @GetMapping("/list")
    @RequiresPermissions("blog:album:list")
    @ResponseBody
    public TableDataInfo list(Album album) {
        startPage();
        List<Album> list = albumService.selectAlbumList(album);
        return getDataTable(list);
    }

    @GetMapping("/add")
    public String add() {
        return "blog/album/add";
    }

    @Log(title = "专辑管理", businessType = BusinessType.INSERT)
    @RequiresPermissions("blog:album:add")
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Album album) {
        return toAjax(albumService.insertAlbum(album));
    }

    @GetMapping("/edit/{albumId}")
    public String edit(@PathVariable Integer albumId, Model model) {
        model.addAttribute("album", albumService.selectAlbumById(albumId));
        return "blog/album/edit";
    }

    @PutMapping("/edit")
    @RequiresPermissions("blog:album:edit")
    @Log(title = "专辑管理", businessType = BusinessType.UPDATE)
    @ResponseBody
    public AjaxResult editSave(Album album) {
        return toAjax(albumService.updateAlbum(album));
    }

    @DeleteMapping("/remove")
    @Log(title = "专辑管理", businessType = BusinessType.DELETE)
    @RequiresPermissions("blog:album:remove")
    @ResponseBody
    public AjaxResult remove(Integer[] ids) {
        return toAjax(albumService.deleteAlbumByIds(ids));
    }

    @PutMapping("/support/{support}")
    @RequiresPermissions("blog:album:support")
    @Log(title = "专辑管理", businessType = BusinessType.UPDATE)
    @ResponseBody
    public AjaxResult supportSave(Integer albumId, @PathVariable String support) {
        return toAjax(albumService.updateAlbumSupportById(albumId, support));
    }

    @PostMapping("/checkAlbumNameUnique")
    @ResponseBody
    public String checkAlbumTitleUnique(String name) {
        return albumService.checkAlbumNameUnique(name);
    }

    @GetMapping("/img")
    public String img() {
        return "blog/album/img";
    }

}
