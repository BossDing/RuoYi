/*
 * Copyright (C) 2017--2019, 微笑的丁总 All rights reserved
 * Author: DingJie
 * FileName: BaiJiaHaoResponse
 * Date: 2019/5/11 12:20 PM
 * Description: 百家号推送结果
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号             描述
 */
package com.ruoyi.common.utils.baidu;

/**
 * 〈一句话功能简述〉<br> 百家号推送结果
 *
 * @author dogjay
 * @create 2019/5/11
 * @since 1.0.0
 */
public class BaiJiaHaoResponse {

    /**
     * errno : 0 data : {"article_id":"文章对应标示(string类型)"} errmsg :
     */
    private int errno;
    private DataEntity data;
    private String errmsg;

    public void setErrno(int errno) {
        this.errno = errno;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }

    public int getErrno() {
        return errno;
    }

    public DataEntity getData() {
        return data;
    }

    public String getErrmsg() {
        return errmsg;
    }

    public class DataEntity {
        /**
         * article_id : 文章对应标示(string类型)
         */
        private String article_id;

        public void setArticle_id(String article_id) {
            this.article_id = article_id;
        }

        public String getArticle_id() {
            return article_id;
        }
    }

    @Override
    public String toString() {
        return "BaiJiaHaoResponse{" +
                "errno=" + errno +
                ", data=" + data.getArticle_id() +
                ", errmsg='" + errmsg + '\'' +
                '}';
    }
}
