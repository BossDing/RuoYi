/*
 * Copyright (C) 2017--2019, 微笑的丁总 All rights reserved
 * Author: DingJie
 * FileName: PublishDto
 * Date: 2019/5/11 12:30 PM
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号             描述
 */
package com.ruoyi.common.utils.baidu;

/**
 * 〈一句话功能简述〉<br>
 *
 * @author dogjay
 * @create 2019/5/11
 * @since 1.0.0
 */
public class PublishDto {

    /**
     * origin_url : http://baijiahao.baidu.com/s?id=1565707368004927
     * is_original : 1
     * app_token : INPUT_TOKEN_HERE
     * cover_images : [{"src":"https://pic.rmb.bdstatic.com/0a9d9bb7c79f1d884a1e3f2cf67840d9.jpeg"},{"src":"https://pic.rmb.bdstatic.com/9be6b6a7aa6d77782e55451d3b5e2e05.jpeg"},{"src":"https://pic.rmb.bdstatic.com/a7b60cf66811e27d7f7466f88ba176e5.jpeg"}]
     * title : 北京日报联合百家号，内容、技术相互赋能
     * app_id : INPUT_ID_HERE
     * content : <p>12月14日，<strong>本句会加粗：北京日报集团与百家号战略合作发布会在京举行。</strong>北京市委副秘书长、宣传部副部长余俊生，北京日报社总编辑伍义林及北京市相关区县宣传部、融媒体中心领导、百度副总裁、百度百家号总经理等参加启动仪式并致辞。本次发布会以“融媒聚势，智造未来”为主题，双方将依托各自在内容、技术、资源等层面的强力优势，开展关于内容生产、分发与内容生态布局等多层次的战略合作，全面打通百家号与北京日报的资源、技术、内容优势环路，构建本地优质内容生态。<a href="https://baidu.com">标签内的链接会被过滤，文本会保留。</a><br/><img src="https://qiniu.pcfast.org/wp-content/uploads/2018/12/2018122110084757.jpeg" alt="签约" /></p><p>北京日报拥有强大的采编能力和雄厚的地方媒体资源，是雄踞本地内容生产端的头部力量，而百家号作为百度内容生态的战略产品，信息流正是双方内容生态的天然连接器，二者的战略联手势必加速传统媒体与内容分发平台的充分融合，赋能信息资讯精准化、个性化和多元化的传播。<b>本句也会加粗：北京日报社党组副书记也在发言中谈到，百家号与北京日报的联合，将百度的先进技术基因注入党媒，实现多介质多渠道的立体传播，一定能够让北京声音在网络上传得更开更广。</b></p>这里是视频(每篇文章只支持发布一篇视频):<video src="http://baijiahao.baidu.com/1561213.mp4" poster="https://http://baijiahao.baidu.com/1561213.png"/>这里是音频（每篇文章只支持发布一篇音频）: <audio src="http://baijiahao.baidu.com/1561213.mp3" title="1561213"/>
     */
    private String origin_url;
    private int is_original;
    private String app_token;
    private String cover_images;
    private String title;
    private String app_id;
    private String content;

    public void setOrigin_url(String origin_url) {
        this.origin_url = origin_url;
    }

    public void setIs_original(int is_original) {
        this.is_original = is_original;
    }

    public void setApp_token(String app_token) {
        this.app_token = app_token;
    }

    public void setCover_images(String cover_images) {
        this.cover_images = cover_images;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setApp_id(String app_id) {
        this.app_id = app_id;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getOrigin_url() {
        return origin_url;
    }

    public int getIs_original() {
        return is_original;
    }

    public String getApp_token() {
        return app_token;
    }

    public String getCover_images() {
        return cover_images;
    }

    public String getTitle() {
        return title;
    }

    public String getApp_id() {
        return app_id;
    }

    public String getContent() {
        return content;
    }

    @Override
    public String toString() {
        return "PublishDto{" +
                "origin_url='" + origin_url + '\'' +
                ", is_original=" + is_original +
                ", app_token='" + app_token + '\'' +
                ", cover_images='" + cover_images + '\'' +
                ", title='" + title + '\'' +
                ", app_id='" + app_id + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
