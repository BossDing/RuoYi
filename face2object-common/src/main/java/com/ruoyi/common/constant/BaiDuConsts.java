/*
 * Copyright (C) 2017--2019, 微笑的丁总 All rights reserved
 * Author: DingJie
 * FileName: BaiDuConsts
 * Date: 2019/5/11 12:15 PM
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号             描述
 */
package com.ruoyi.common.constant;

/**
 * 百度相关常量<br>
 *
 * @author dogjay
 * @create 2019/5/11
 * @since 1.0.0
 */
public class BaiDuConsts {
    public static final String BAIJIAHAO_APP_ID = "1605866704879494";

    public static final String BAIJIAHAO_APP_TOKEN = "75aadf24d0357587fa08707738ffd75b";

    public static final String ARTICLE_PUBLISH_URL = "http://baijiahao.baidu.com/builderinner/open/resource/article/publish";

    public static final String ARTICLE_GALLERY_URL = "http://baijiahao.baidu.com/builderinner/open/resource/article/gallery";

    public static final String VIDEO_PUBLISH_URL = "http://baijiahao.baidu.com/builderinner/open/resource/video/publish";

    public static final String ARTICLE_WITHDRAW_URL = "http://baijiahao.baidu.com/builderinner/open/resource/article/withdraw";

    public static final String ARTICLE_REPUBLISH_URL = "http://baijiahao.baidu.com/builderinner/open/resource/article/republish";

    public static final String ORIGIN_STATIC_URL = "https://www.bossding.com.cn/article/";

    public static final String XiongZhangHao_URL_DAY = "http://data.zz.baidu.com/urls?appid=1605866704879494&token=ZuC9yhHxLjOMZdyl&type=realtime";
    public static final String XiongZhangHao_URL_WEEK = "http://data.zz.baidu.com/urls?appid=1605866704879494&token=ZuC9yhHxLjOMZdyl&type=realtime";
}
