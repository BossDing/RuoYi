package com.ruoyi.common.constant;

/**
 * @className: AlbumConstants
 * @description: 分类常量信息
 * @auther: DogJay
 * @date: 03/25/19
 * @version: 1.0
 */
public class AlbumConstants {
    /**
     * 分类名称是否唯一
     */
    public final static String ALBUM_TITLE_UNIQUE = "0";
    public final static String ALBUM_TITLE_NOT_UNIQUE = "1";
}
