package com.ruoyi.common.enums;

/**
 * @author yadong.zhang (yadong.zhang0415(a)gmail.com)
 * @version 1.0
 * @website https://www.zhyd.me
 * @date 2018/4/16 16:26
 * @since 1.0
 */
public enum FileUploadType {
    COMMON("bosdding/"),
    QRCODE("bosdding/qrcode/"),
    SIMPLE("bosdding/article/"),
    COVER_IMAGE("bosdding/cover/"),
    EMOJI_IMAGE("bosdding/emoji/");

    private String path;

    FileUploadType(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }
}
