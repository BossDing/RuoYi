/*
 * Copyright (C) 2017--2019, 微笑的丁总 All rights reserved
 * Author: DingJie
 * FileName: Comment
 * Date: 2019/5/2 12:50 AM
 * Description: 评论
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号             描述
 */
package com.ruoyi.blog.domain;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * 〈一句话功能简述〉<br> 评论
 *
 * @author dogjay
 * @create 2019/5/2
 * @since 1.0.0
 */
@Data
public class Comment extends BaseEntity {

    private static final long serialVersionUID = 1L;
    private Long commentId;
    private String content;
    private Long articleId;
    private String email;
    private String site;
    private String avatar;
    private String orderType;
    private String ip;
    private String city;
    private String browser;
}
