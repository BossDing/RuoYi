/*
 * Copyright (C) 2017--2019, 微笑的丁总 All rights reserved
 * Author: DingJie
 * FileName: Album
 * Date: 2019/5/15 2:53 PM
 * Description: 专辑
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号             描述
 */
package com.ruoyi.blog.domain;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * 〈一句话功能简述〉<br> 专辑
 *
 * @author dogjay
 * @create 2019/5/15
 * @since 1.0.0
 */
@Data
public class Album extends BaseEntity {
    private static final long serialVersionUID = 1L;

    private Integer albumId;

    private String name;

    private String description;

    private String image;

    private Integer count;

    private String support;
    
}
