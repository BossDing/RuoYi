package com.ruoyi.blog.domain;

import lombok.Data;

import java.util.List;

/**
 * @className: Archives
 * @description: 归档VO
 * @auther: DogJay
 * @date: 04/02/19
 * @version: 1.0
 */
@Data
public class Archives {
    private String date;
    private Integer count;
    private List<Blog> blogs;
}
