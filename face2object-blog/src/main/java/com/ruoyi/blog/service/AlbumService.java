/*
 * Copyright (C) 2017--2019, 微笑的丁总 All rights reserved
 * Author: DingJie
 * FileName: AlbumService
 * Date: 2019/5/15 3:12 PM
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号             描述
 */
package com.ruoyi.blog.service;

import com.ruoyi.blog.domain.Album;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 *
 * @author dogjay
 * @create 2019/5/15
 * @since 1.0.0
 */
public interface AlbumService {
    /**
     * 根据条件选择Album集合
     *
     * @param album 带有条件的album
     * @return 符合条件的album实体类
     */
    List<Album> selectAlbumList(Album album);

    List<Album> selectFrontAlbumList();

    /**
     * 新增Album
     *
     * @param album 需要新增的Album实体类的信息
     * @return 受影响的行数
     */
    int insertAlbum(Album album);

    /**
     * 根据id获取Album的信息
     *
     * @param albumId 需要获取得Album实体类的id
     * @return Album实体类
     */
    Album selectAlbumById(Integer albumId);

    /**
     * 更新Album
     *
     * @param album 需要更新的Album实体类的信息
     * @return 受影响的行数
     */
    int updateAlbum(Album album);

    /**
     * 批量删除album
     *
     * @param ids 需要删除的album的id
     * @return 受影响的行数
     */
    int deleteAlbumByIds(Integer[] ids);

    /**
     * 根据id更新推荐状态
     *
     * @param albumId 需要更新的id
     * @param support 需要切换的状态
     * @return 受影响的行数
     */
    int updateAlbumSupportById(@Param("albumId") Integer albumId, @Param("support") String support);

    /**
     * 更新文章数量
     *
     * @param albumId
     * @return
     */
    int updateAlbumCountById(@Param("albumId") Integer albumId);

    int minusAlbumCountById(@Param("albumId") Integer albumId);

    /**
     * 根据title获取album实体
     *
     * @param name 需要的album的标题
     * @return album实体
     */
    Album selectAlbumByAlbumName(String name);

    /**
     * 获取推荐的分类
     *
     * @return
     */
    List<Album> selectSupportAlbumList();


    /**
     * 校验分类名称是否唯一
     *
     * @param name 分类名称
     * @return 结果
     */
    String checkAlbumNameUnique(String name);
}
