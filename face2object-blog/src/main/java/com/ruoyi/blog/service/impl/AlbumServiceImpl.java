/*
 * Copyright (C) 2017--2019, 微笑的丁总 All rights reserved
 * Author: DingJie
 * FileName: AlbumServiceImpl
 * Date: 2019/5/15 3:13 PM
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号             描述
 */
package com.ruoyi.blog.service.impl;

import com.ruoyi.blog.domain.Album;
import com.ruoyi.blog.mapper.AlbumMapper;
import com.ruoyi.blog.service.AlbumService;
import com.ruoyi.common.constant.AlbumConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 *
 * @author dogjay
 * @create 2019/5/15
 * @since 1.0.0
 */
@Service
public class AlbumServiceImpl implements AlbumService {
    @Autowired
    AlbumMapper albumMapper;

    /**
     * 根据条件选择Album集合
     *
     * @param album 带有条件的album
     * @return 符合条件的album实体类
     */
    @Override
    public List<Album> selectAlbumList(Album album) {
        return albumMapper.selectAlbumList(album);
    }
    
    @Override
    public List<Album> selectFrontAlbumList() {
//        List<Album> albumList = (List<Album>) CacheUtils.get("albumCache",CachePrefix.FRONT_ALBUM);
//        if(null==albumList){
//            albumList = albumMapper.selectFrontAlbumList();
//            CacheUtils.put("albumCache",CachePrefix.FRONT_ALBUM,albumList);
//        }
//        return albumList;
        return albumMapper.selectFrontAlbumList();
    }

    /**
     * 新增Album
     *
     * @param album 需要新增的Album实体类的信息
     * @return 受影响的行数
     */
    @Override
    public int insertAlbum(Album album) {
        return albumMapper.insertAlbum(album);
    }

    /**
     * 根据id获取Album的信息
     *
     * @param albumId 需要获取得Album实体类的id
     * @return Album实体类
     */
    @Override
    public Album selectAlbumById(Integer albumId) {
        return albumMapper.selectAlbumById(albumId);
    }

    /**
     * 更新Album
     *
     * @param album 需要更新的Album实体类的信息
     * @return 受影响的行数
     */
    @Override
    public int updateAlbum(Album album) {
        return albumMapper.updateAlbum(album);
    }

    /**
     * 批量删除album
     *
     * @param ids 需要删除的album的id
     * @return 受影响的行数
     */
    @Override
    public int deleteAlbumByIds(Integer[] ids) {
        return albumMapper.deleteAlbumByIds(ids);
    }

    /**
     * 根据id更新推荐状态
     *
     * @param albumId 需要更新的id
     * @param support 需要切换的状态
     * @return 受影响的行数
     */
    @Override
    public int updateAlbumSupportById(Integer albumId, String support) {
        return albumMapper.updateAlbumSupportById(albumId, support);
    }

    /**
     * 更新文章数量
     *
     * @param albumId
     * @return
     */
    @Override
    public int updateAlbumCountById(Integer albumId) {
        return albumMapper.updateAlbumCountById(albumId);
    }

    /**
     * @param albumId
     * @return
     */
    @Override
    public int minusAlbumCountById(Integer albumId) {
        return albumMapper.minusAlbumCountById(albumId);
    }

    /**
     * 根据title获取album实体
     *
     * @param name 需要的album的标题
     * @return album实体
     */
    @Override
    public Album selectAlbumByAlbumName(String name) {
        return albumMapper.selectAlbumByAlbumName(name);
    }

    /**
     * 获取推荐的分类
     *
     * @return
     */
    @Override
    public List<Album> selectSupportAlbumList() {
        return albumMapper.selectSupportAlbumList();
    }

    /**
     * 校验分类名称是否唯一
     *
     * @param name 分类名称
     * @return 结果
     */
    @Override
    public String checkAlbumNameUnique(String name) {
        Album album = albumMapper.selectAlbumByAlbumName(name);
        return album == null ? AlbumConstants.ALBUM_TITLE_UNIQUE : AlbumConstants.ALBUM_TITLE_NOT_UNIQUE;
    }
}
