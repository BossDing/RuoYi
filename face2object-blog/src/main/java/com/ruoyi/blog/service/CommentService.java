/*
 * Copyright (C) 2017--2019, 微笑的丁总 All rights reserved
 * Author: DingJie
 * FileName: CommentService
 * Date: 2019/5/2 1:16 AM
 * Description: 评论业务处理层
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号             描述
 */
package com.ruoyi.blog.service;

import com.ruoyi.blog.domain.Comment;

import java.util.List;

/**
 * 〈一句话功能简述〉<br> 评论业务处理层
 *
 * @author dogjay
 * @create 2019/5/2
 * @since 1.0.0
 */
public interface CommentService {

    List<Comment> selectCommentList(Comment comment);

    /**
     * 新增分类
     *
     * @param comment 分类实体类
     * @return 受影响的行数
     */
    int insertComment(Comment comment);

    /**
     * 根据id获取comment实体类
     *
     * @param commentId 分类的id
     * @return comment实体类
     */
    Comment selectCommentById(Integer commentId);

    /**
     * 根据id批量删除comment
     *
     * @param ids 需要删除的comment的id
     * @return 受影响的行数
     */
    int deleteCommentByIds(Integer[] ids);

    /**
     * @param articleId
     * @return
     */
    List<Comment> selectCommentByBlogId(Integer articleId);

}
