package com.ruoyi.blog.service;

import com.ruoyi.blog.domain.Archives;
import com.ruoyi.blog.domain.Blog;

import java.util.List;

/**
 * @className: HomeService
 * @description:
 * @auther: DogJay
 * @date: 03/27/19
 * @version: 1.0
 */
public interface HomeService {
    /**
     * 获取最新的blog
     *
     * @param blog
     * @return 最新的blog的集合
     */
    List<Blog> selectFrontBlogList(Blog blog);

    /**
     * 获取Album
     *
     * @param blog
     * @return
     */
    List<Blog> selectAlbumBlogList(Blog blog);

    /**
     * 获取归档信息
     *
     * @return 归档集合
     */
    List<Archives> selectArchives();
}
