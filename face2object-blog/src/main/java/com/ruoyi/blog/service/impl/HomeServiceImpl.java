package com.ruoyi.blog.service.impl;

import com.ruoyi.blog.domain.Archives;
import com.ruoyi.blog.domain.Blog;
import com.ruoyi.blog.mapper.BlogMapper;
import com.ruoyi.blog.service.HomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @className: HomeServiceImpl
 * @description:
 * @auther: DogJay
 * @date: 03/27/19
 * @version: 1.0
 */
@Service
public class HomeServiceImpl implements HomeService {

    @Autowired
    BlogMapper blogMapper;

    @Override
    public List<Blog> selectFrontBlogList(Blog blog) {
        List<Blog> blogs = blogMapper.selectFrontBlogList(blog);
        return blogs;
    }

    /**
     * 获取Album
     *
     * @param blog
     * @return
     */
    @Override
    public List<Blog> selectAlbumBlogList(Blog blog) {
        List<Blog> blogs = blogMapper.selectAlbumBlogList(blog);
        return blogs;
    }

    @Override
    public List<Archives> selectArchives() {
        List<Archives> archives = blogMapper.selectArchivesDateAndCount();
        for (Archives archive : archives) {
            archive.setBlogs(blogMapper.selectBlogByCreateTime(archive.getDate()));
        }
        return archives;
    }
}
