/*
 * Copyright (C) 2017--2019, 微笑的丁总 All rights reserved
 * Author: DingJie
 * FileName: CommentServiceImpl
 * Date: 2019/5/2 1:27 AM
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号             描述
 */
package com.ruoyi.blog.service.impl;

import com.ruoyi.blog.domain.Comment;
import com.ruoyi.blog.mapper.CommentMapper;
import com.ruoyi.blog.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 *
 * @author dogjay
 * @create 2019/5/2
 * @since 1.0.0
 */
@Service
public class CommentServiceImpl implements CommentService {
    @Autowired
    CommentMapper commentMapper;
    @Override
    public List<Comment> selectCommentList(Comment comment) {
        return commentMapper.selectCommentList(comment);
    }

    /**
     * 新增分类
     *
     * @param comment 分类实体类
     * @return 受影响的行数
     */
    @Override
    public int insertComment(Comment comment) {
        return commentMapper.insertComment(comment);
    }

    /**
     * 根据id获取comment实体类
     *
     * @param commentId 分类的id
     * @return comment实体类
     */
    @Override
    public Comment selectCommentById(Integer commentId) {
        return commentMapper.selectCommentById(commentId);
    }

    /**
     * 根据id批量删除comment
     *
     * @param ids 需要删除的comment的id
     * @return 受影响的行数
     */
    @Override
    public int deleteCommentByIds(Integer[] ids) {
        return commentMapper.deleteCommentByIds(ids);
    }

    /**
     * @param articleId
     * @return
     */
    @Override
    public List<Comment> selectCommentByBlogId(Integer articleId) {
        return commentMapper.selectCommentByBlogId(articleId);
    }
}
