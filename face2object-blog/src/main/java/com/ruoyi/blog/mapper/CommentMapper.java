/*
 * Copyright (C) 2017--2019, 微笑的丁总 All rights reserved
 * Author: DingJie
 * FileName: CommentMapper
 * Date: 2019/5/2 12:57 AM
 * Description: 评论映射
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号             描述
 */
package com.ruoyi.blog.mapper;

import com.ruoyi.blog.domain.Comment;

import java.util.List;

/**
 * 〈一句话功能简述〉<br> 评论映射
 *
 * @author dogjay
 * @create 2019/5/2
 * @since 1.0.0
 */
public interface CommentMapper {
    /**
     * 根据条件选择Comment集合
     *
     * @param comment 带有条件的comment
     * @return 符合条件的comment实体类
     */
    List<Comment> selectCommentList(Comment comment);

    /**
     * 新增Comment
     *
     * @param comment 需要新增的Comment实体类的信息
     * @return 受影响的行数
     */
    int insertComment(Comment comment);

    /**
     * 根据id获取Comment的信息
     *
     * @param commentId 需要获取得Comment实体类的id
     * @return Comment实体类
     */
    Comment selectCommentById(Integer commentId);

    /**
     * 批量删除comment
     *
     * @param ids 需要删除的comment的id
     * @return 受影响的行数
     */
    int deleteCommentByIds(Integer[] ids);

    /**
     * @param articleId
     * @return
     */
    List<Comment> selectCommentByBlogId(Integer articleId);
}
