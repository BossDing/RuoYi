package com.ruoyi.dashboard.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.dashboard.domain.BusinessCommonData;
import com.ruoyi.dashboard.domain.LogMessage;
import com.ruoyi.dashboard.domain.VisitCount;
import com.ruoyi.dashboard.service.DashboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @className: DashboardServiceImpl
 * @description:
 * @auther: DogJay
 * @date: 04/02/19
 * @version: 1.0
 */
@Service
public class DashboardServiceImpl implements DashboardService {
//    @Autowired
//    VisitLogMapper visitLogMapper;
//    @Autowired
//    OperLogMapper operLogMapper;
//    @Autowired
//    LogininforMapper logininforMapper;
//    @Autowired
//    JobLogMapper jobLogMapper;
//    @Autowired
//    DictDataMapper dictDataMapper;


    @Override
    public List<BusinessCommonData> selectSpiderData() {
        return null;
    }

    @Override
    public List<VisitCount> getVisitData() {
        List<VisitCount> visitCounts = new ArrayList<>();
        
        return visitCounts;
    }

    @Override
    public List<LogMessage> selectLogMessage() {
        List<LogMessage> messages = new ArrayList<>();
        
        return messages;
    }

}
