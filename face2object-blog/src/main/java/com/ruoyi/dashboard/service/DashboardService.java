package com.ruoyi.dashboard.service;

import com.ruoyi.dashboard.domain.BusinessCommonData;
import com.ruoyi.dashboard.domain.LogMessage;
import com.ruoyi.dashboard.domain.VisitCount;

import java.util.List;

/**
 * @className: com.bossding.project.dashboard.business
 * @description:
 * @auther: DogJay
 * @date: 04/02/19
 * @version: 1.0
 */
public interface DashboardService {
    /**
     * 获取爬虫访问记录
     *
     * @return 爬虫数据实体类
     */
    List<BusinessCommonData> selectSpiderData();

    /**
     * 获取访问量的数据
     *
     * @return VisitCount实体类
     */
    List<VisitCount> getVisitData();

    List<LogMessage> selectLogMessage();
}
