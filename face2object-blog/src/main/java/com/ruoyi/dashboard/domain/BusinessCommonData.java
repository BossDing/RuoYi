package com.ruoyi.dashboard.domain;

import lombok.Data;

/**
 * @className: BusinessCommonData
 * @description:
 * @auther: DogJay
 * @date: 04/02/19
 * @version: 1.0
 */
@Data
public class BusinessCommonData {
    /**
     * 名称
     */
    private String name;
    /**
     * 值
     */
    private Integer value;
}
