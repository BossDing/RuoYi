package com.ruoyi.system.domain;

import lombok.Data;

/**
 * @className: Business
 * @description:
 * @auther: DogJay
 * @date: 04/08/19
 * @version: 1.0
 */
@Data
public class Business {
    String name;
    Object value;
}
